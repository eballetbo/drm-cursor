/*
 * Copyright © 2017 Collabora Ltd.
 *
 * This file is part of vkmark.
 *
 * vkmark is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * vkmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with vkmark. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *   Alexandros Frantzis <alexandros.frantzis@collabora.com>
 */

#pragma once

#include "managed_resource.h"

#include <vector>

#include <xf86drmMode.h>
#include <gbm.h>
#include <linux/vt.h>

class VTState
{
public:
    VTState();
    ~VTState();
    void restore() const;

private:
    ManagedResource<int> const vt_fd;
    vt_mode prev_vt_mode;
};

class KMSWindowSystem
{
public:
    KMSWindowSystem(std::string const& drm_device);
    ~KMSWindowSystem();

    uint32_t width() { return crtc_width; }
    uint32_t height() { return crtc_height; }

    ManagedResource<gbm_bo*> create_cursor_gbm_bo(uint32_t width, uint32_t height);

    virtual void set_cursor(gbm_bo* gbm_bo);
    virtual void move_cursor(int x, int y);

protected:
    ManagedResource<gbm_bo*> create_bo(uint32_t width, uint32_t height,
                                       uint32_t format, uint32_t flags);
    ManagedResource<uint32_t> create_drm_fb(gbm_bo* gbm_bo);
    void clear();

    ManagedResource<int> const drm_fd;
    ManagedResource<drmModeResPtr> const drm_resources;
    ManagedResource<drmModeConnectorPtr> const drm_connector;
    ManagedResource<drmModeCrtcPtr> const drm_prev_crtc;
    ManagedResource<drmModeCrtcPtr> const drm_crtc;
    ManagedResource<gbm_device*> const gbm;
    uint32_t const crtc_width;
    uint32_t const crtc_height;
    VTState const vt_state;
    ManagedResource<gbm_bo*> background_bo;
    ManagedResource<uint32_t> background_fb;
};
