/*
 * Copyright © 2017 Collabora Ltd.
 *
 * This file is part of vkmark.
 *
 * vkmark is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * vkmark is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with vkmark. If not, see <http://www.gnu.org/licenses/>.
 *
 * Authors:
 *   Alexandros Frantzis <alexandros.frantzis@collabora.com>
 */

#include "kms_window_system.h"

#include "log.h"

#include <xf86drm.h>
#include <drm_fourcc.h>

#include <system_error>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <sys/ioctl.h>
#include <csignal>
#include <cstring>

namespace
{

ManagedResource<int> open_drm_device(std::string const& drm_device)
{
    int drm_fd = open(drm_device.c_str(), O_RDWR);
    if (drm_fd < 0)
    {
        throw std::system_error{
            errno,
            std::system_category(),
            "Failed to open drm device"};
    }

    return ManagedResource<int>{std::move(drm_fd), close};
}

ManagedResource<drmModeResPtr> get_resources_for(int drm_fd)
{
    return ManagedResource<drmModeResPtr>{
        drmModeGetResources(drm_fd), drmModeFreeResources};
}

ManagedResource<drmModeConnectorPtr> get_connector_with_id(int drm_fd, uint32_t connector_id)
{
    return ManagedResource<drmModeConnectorPtr>{
        drmModeGetConnector(drm_fd, connector_id), drmModeFreeConnector};
}


ManagedResource<drmModeEncoderPtr> get_encoder_with_id(int drm_fd, uint32_t encoder_id)
{
    return ManagedResource<drmModeEncoderPtr>{
        drmModeGetEncoder(drm_fd, encoder_id), drmModeFreeEncoder};
}

ManagedResource<drmModeCrtcPtr> get_crtc_with_id(int drm_fd, uint32_t crtc_id)
{
    return ManagedResource<drmModeCrtcPtr>{
        drmModeGetCrtc(drm_fd, crtc_id), drmModeFreeCrtc};
}

ManagedResource<drmModeConnectorPtr> get_connected_connector(
    int drm_fd, drmModeResPtr resources)
{
    for (int c = 0; c < resources->count_connectors; c++)
    {
        auto connector = get_connector_with_id(drm_fd, resources->connectors[c]);

        if (connector->connection == DRM_MODE_CONNECTED)
        {
            Log::debug("KMSWindowSystem: Using connector %d\n",
                       connector->connector_id);
            return connector;
        }
    }

    throw std::runtime_error{"Failed to find a connected drm connector"};
}

bool is_encoder_available(int drm_fd, drmModeResPtr resources, uint32_t encoder_id)
{
    for (int c = 0; c < resources->count_connectors; c++)
    {
        auto connector = get_connector_with_id(drm_fd, resources->connectors[c]);

        if (connector->encoder_id == encoder_id &&
            connector->connection == DRM_MODE_CONNECTED)
        {
            auto encoder = get_encoder_with_id(drm_fd, connector->encoder_id);
            if (encoder->crtc_id)
                return false;
        }
    }

    return true;
}

bool is_crtc_available(int drm_fd, drmModeResPtr resources, uint32_t crtc_id)
{
    for (int c = 0; c < resources->count_connectors; c++)
    {
        auto connector = get_connector_with_id(drm_fd, resources->connectors[c]);

        if (connector->encoder_id &&
            connector->connection == DRM_MODE_CONNECTED)
        {
            auto encoder = get_encoder_with_id(drm_fd, connector->encoder_id);
            if (encoder->crtc_id == crtc_id)
                return false;
        }
    }

    return true;
}

std::vector<ManagedResource<drmModeEncoderPtr>> get_available_encoders_for_connector(
    int drm_fd, drmModeResPtr resources, drmModeConnectorPtr connector)
{
    std::vector<ManagedResource<drmModeEncoderPtr>> encoders;

    for (int e = 0; e < connector->count_encoders; e++)
    {
        auto const encoder_id = connector->encoders[e];
        if (is_encoder_available(drm_fd, resources, encoder_id))
            encoders.push_back(get_encoder_with_id(drm_fd, encoder_id));
    }

    return encoders;
}

bool does_encoder_support_crtc_index(drmModeEncoderPtr encoder, uint32_t crtc_index)
{
    return encoder->possible_crtcs & (1 << crtc_index);
}

ManagedResource<drmModeCrtcPtr> get_current_crtc_for_connector(
    int drm_fd, drmModeConnectorPtr connector)
{
    if (connector->encoder_id)
    {
        auto const encoder = get_encoder_with_id(drm_fd, connector->encoder_id);
        if (encoder->crtc_id)
            return get_crtc_with_id(drm_fd, encoder->crtc_id);
    }

    return ManagedResource<drmModeCrtcPtr>{
        new drmModeCrtc{},
        [] (auto& c) { delete c; }};
}

ManagedResource<drmModeCrtcPtr> get_configured_crtc_with_id(
    int drm_fd, drmModeConnectorPtr connector, uint32_t crtc_id)
{
    auto crtc = get_crtc_with_id(drm_fd, crtc_id);
    crtc->mode = {};

    // Use the preferred mode, otherwise the largest available
    for (int m = 0; m < connector->count_modes; ++m)
    {
        auto const& mode_info = connector->modes[m];
        if (mode_info.type & DRM_MODE_TYPE_PREFERRED)
        {
            crtc->mode = mode_info;
            break;
        }

        auto const mode_pixels = mode_info.hdisplay * mode_info.vdisplay;
        if (mode_pixels > crtc->mode.hdisplay * crtc->mode.vdisplay)
            crtc->mode = mode_info;
    }

    Log::debug("KMSWindowSystem: Using crtc mode %dx%d%s\n",
               crtc->mode.hdisplay,
               crtc->mode.vdisplay,
               crtc->mode.type & DRM_MODE_TYPE_PREFERRED ? " (preferred)" : "");
    return crtc;
}

ManagedResource<drmModeCrtcPtr> get_crtc_for_connector(
    int drm_fd, drmModeResPtr resources, drmModeConnectorPtr connector)
{
    if (connector->encoder_id)
    {
        auto const encoder = get_encoder_with_id(drm_fd, connector->encoder_id);
        if (encoder->crtc_id)
        {
            Log::debug("KMSWindowSystem: Using already attached encoder %d, crtc %d\n",
                       encoder->encoder_id,
                       encoder->crtc_id);
            return get_configured_crtc_with_id(drm_fd, connector, encoder->crtc_id);
        }
    }

    Log::debug("KMSWindowSystem: No crtc/encoder attached to connector, "
               "trying to attach\n");

    auto const available_encoders = get_available_encoders_for_connector(
        drm_fd, resources, connector);

    for (int c = 0; c < resources->count_crtcs; c++)
    {
        auto const crtc_id = resources->crtcs[c];

        Log::debug("KMSWindowSystem: Trying crtc %d\n", crtc_id);

        if (is_crtc_available(drm_fd, resources, crtc_id))
        {
            for (auto const& encoder : available_encoders)
            {
                if (does_encoder_support_crtc_index(encoder, c))
                {
                    return get_configured_crtc_with_id(drm_fd, connector, crtc_id);
                }
            }
        }
    }

    throw std::runtime_error{"Failed to get usable crtc"};
}

ManagedResource<gbm_device*> create_gbm_device(int drm_fd)
{
    auto gbm_raw = gbm_create_device(drm_fd);
    if (!gbm_raw)
        throw std::runtime_error{"Failed to create gbm device"};

    return ManagedResource<gbm_device*>{std::move(gbm_raw), gbm_device_destroy};

}

ManagedResource<int> open_active_vt()
{
    auto fd = open("/dev/tty0", O_RDONLY);
    if (fd < 0)
        throw std::runtime_error{"Failed to open active VT"};

    return ManagedResource<int>{std::move(fd), close};
}

void page_flip_handler(int, unsigned int, unsigned int, unsigned int, void*)
{
}

VTState* global_vt_state = nullptr;

void restore_vt(int)
{
    if (global_vt_state)
        global_vt_state->restore();
}

}

VTState::VTState()
    : vt_fd{open_active_vt()}
{
    if (ioctl(vt_fd, VT_GETMODE, &prev_vt_mode) < 0)
    {
        throw std::system_error{
            errno, std::system_category(), "Failed to get VT control mode"};
    }

    vt_mode const vtm{VT_PROCESS, 0, 0, 0, 0};

    if (ioctl(vt_fd, VT_SETMODE, &vtm) < 0)
    {
        throw std::system_error{
            errno, std::system_category(), "Failed to set VT process control mode"};
    }

    global_vt_state = this;

    struct sigaction sa{};
    sa.sa_handler = restore_vt;

    sigaction(SIGSEGV, &sa, nullptr);
    sigaction(SIGABRT, &sa, nullptr);
}

void VTState::restore() const
{
    if (prev_vt_mode.mode == VT_AUTO)
        ioctl(vt_fd, VT_SETMODE, &prev_vt_mode);
}

VTState::~VTState()
{
    restore();

    struct sigaction sa{};
    sa.sa_handler = SIG_DFL;

    sigaction(SIGSEGV, &sa, nullptr);
    sigaction(SIGABRT, &sa, nullptr);

    global_vt_state = nullptr;
}

KMSWindowSystem::KMSWindowSystem(std::string const& drm_device)
    : drm_fd{open_drm_device(drm_device)},
      drm_resources{get_resources_for(drm_fd)},
      drm_connector{get_connected_connector(drm_fd, drm_resources)},
      drm_prev_crtc{get_current_crtc_for_connector(drm_fd, drm_connector)},
      drm_crtc{get_crtc_for_connector(drm_fd, drm_resources, drm_connector)},
      gbm{create_gbm_device(drm_fd)},
      crtc_width{drm_crtc->mode.hdisplay},
      crtc_height{drm_crtc->mode.vdisplay}
{
    clear();
}

KMSWindowSystem::~KMSWindowSystem()
{
    drmModeSetCrtc(
        drm_fd, drm_prev_crtc->crtc_id, drm_prev_crtc->buffer_id,
        drm_prev_crtc->x, drm_prev_crtc->y,
        &drm_connector->connector_id, 1,
        &drm_prev_crtc->mode);
}

void KMSWindowSystem::set_cursor(gbm_bo* gbm_bo)
{
    auto const ret = drmModeSetCursor(
        drm_fd, drm_crtc->crtc_id,
        gbm_bo_get_handle(gbm_bo).u32,
        gbm_bo_get_width(gbm_bo), gbm_bo_get_height(gbm_bo));
    if (ret < 0)
        throw std::system_error{-ret, std::system_category(), "Failed to set cursor"};
}

void KMSWindowSystem::move_cursor(int x, int y)
{
    auto const ret = drmModeMoveCursor(drm_fd, drm_crtc->crtc_id, x, y);
    if (ret < 0)
        throw std::system_error{-ret, std::system_category(), "Failed to move cursor"};
}

ManagedResource<gbm_bo*> KMSWindowSystem::create_cursor_gbm_bo(
    uint32_t width, uint32_t height)
{
    return create_bo(width, height, GBM_FORMAT_ARGB8888,
        GBM_BO_USE_CURSOR | GBM_BO_USE_LINEAR | GBM_BO_USE_WRITE);
}

ManagedResource<gbm_bo*> KMSWindowSystem::create_bo(
    uint32_t width, uint32_t height, uint32_t format, uint32_t flags)
{
    auto bo_raw = gbm_bo_create(gbm, width, height, format, flags);

    if (!bo_raw)
        throw std::runtime_error{"Failed to create gbm bo"};

    return ManagedResource<gbm_bo*>{std::move(bo_raw), gbm_bo_destroy};
}

ManagedResource<uint32_t> KMSWindowSystem::create_drm_fb(gbm_bo* gbm_bo)
{
    uint32_t fb = 0;

    uint32_t handles[4] = {gbm_bo_get_handle(gbm_bo).u32, 0, 0, 0};
    uint32_t strides[4] = {gbm_bo_get_stride(gbm_bo), 0, 0, 0};
    uint32_t offsets[4] = {0, 0, 0, 0};

    auto const ret = drmModeAddFB2(
            drm_fd, gbm_bo_get_width(gbm_bo), gbm_bo_get_height(gbm_bo),
            gbm_bo_get_format(gbm_bo), handles, strides, offsets, &fb, 0);

    if (ret < 0)
        throw std::system_error{-ret, std::system_category(), "Failed to add drm fb"};

    return ManagedResource<uint32_t>{
        std::move(fb),
        [this] (auto& fb) { drmModeRmFB(drm_fd, fb); }};
}


void KMSWindowSystem::clear()
{
    background_bo = create_bo(crtc_width, crtc_height,
                              GBM_FORMAT_XRGB8888,
                              GBM_BO_USE_SCANOUT | GBM_BO_USE_LINEAR | GBM_BO_USE_WRITE);

    void* map_data = nullptr;
    uint32_t stride = 0;
    if (!gbm_bo_map(background_bo, 0, 0, crtc_width, crtc_height, GBM_BO_TRANSFER_WRITE,
                    &stride, &map_data))
    {
        throw std::system_error{errno, std::system_category(), "Failed to gbm_bo_map"};
    }

    std::memset(map_data, 0, crtc_height * stride);

    gbm_bo_unmap(background_bo, map_data);

    background_fb = create_drm_fb(background_bo);

    drmModeSetCrtc(
        drm_fd, drm_crtc->crtc_id, background_fb, 0, 0,
        &drm_connector->connector_id,
        1, &drm_crtc->mode);
}

